 libILUStateMachine

## Requirements

You need the following software installed for the build process:

    autoconf 2.60  (or later)
    automake 1.15  (or later)
    libtool  2.4.6 (or later)
    GNU m4 (required by autoconf)

This library depends on the following libraries:
    boost 1.67.0  (or later): when C++14 is not available (boost headers only)

## Build from a release

Building from a release is made in the following steps:

    ./configure
    make
    make check (optional)
    make install

You probably need to be root when doing the last command.

## Build from git

Building from git is made in the following steps:

    autoreconf -i
    ./configure
    make
    make check (optional)
    make install

## Configure options

### Enable/disable
--enable-c14: force using c++14 (no auto-detect).

--disable-c14: do not use c++14 (no auto-detect).

--enable-clang-static-analysis: force using clang static analysis (no auto-detect if clang is available).

--disable-clang-static-analysis: do not use clang static analysis (no auto-detect if clang is available).


### With/without
--with-boost-include-path=<path to boost include dir>: if boost is required (no C++14) the standard and 'prefix' paths will be used to include boost. However, if boost is installed in another location this parameter lets you set the include path where the boost headers can be found.
      
### Prefix
You probably need to be root when doing a 'make install'.

If you want to install libILUStateMachine in a different file hierarchy
than `/usr/local`, specify that when running configure:

    ./configure --prefix=/path/to/libILUStateMachine/tree

If you have write permission in that directory, you can do 'make install'
without being root. An example of this would be to make a local install in
your own home directory:

    ./configure --prefix=$HOME
    make
    make install