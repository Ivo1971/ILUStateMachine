/** @file
 ** @brief 1-file state machine demo using a state machine data instance
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** State machine data class declaration.
 ** One instance of this class is created and provided to all states in their 
 ** constructors.
 **
 ***************************************************************************************/
class CData {
public:
   CData(void)
      : m_strMsg("no message yet")
   {
      LogInfo("[%s][%u] state machine data instance constructed with message [%s]\n", __FUNCTION__, __LINE__, m_strMsg.c_str());
   }

   ~CData(void)
   {
      LogInfo("[%s][%u] state machine data instance destructed with message [%s]\n", __FUNCTION__, __LINE__, m_strMsg.c_str());
   }
   
public:
   std::string GetMsg(void) const
   {
      return m_strMsg;
   }
   
  void SetMsg(const std::string& strMsg)
   {
      m_strMsg = strMsg;
   }
   
private:
   std::string m_strMsg;
};
typedef TYPESEL::shared_ptr<CData> SPData;

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CState2;

/****************************************************************************************
 ** 
 ** First state
 **
 ** This state has 1 event handler. The event ID is an integer (value 1). It calls the
 ** registered handler and then switches to the second state.
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1(SPData spData)
      : CStateEvtId("state-1")
      , m_spData(spData)
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CState1, HandlerEvt1), //< the event handler
                    ToState<CState2, CData>(),          //< the state transition: switch to state-2 when the handler returns
                    1                                   //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created (current message: [%s])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->GetMsg().c_str());
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed (current message: [%s])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->GetMsg().c_str());
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      std::stringstream ss;
      ss << "first event handled with data [" << *pEvtData << "]";
      m_spData->SetMsg(ss.str());
   }

private:
   SPData m_spData;
};

/****************************************************************************************
 ** 
 ** Second state.
 **
 ** This state has 1 event handler. The event ID is an integer (value 1). It calls the
 ** registered handler and then remains in this state.
 **
 ***************************************************************************************/
/** Class definition of state-2
 **/
class CState2 : public CStateEvtId {
public:
   CState2(SPData spData)
      : CStateEvtId("state-2")
      , m_spData(spData)
   {
      //register handlers
      EventRegister(
                    HANDLER(int, CState2, HandlerEvt1), //< the event handler
                    NO_STATE_CHANGE,                    //< the state transition: stay in this state
                    1                                   //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created (current message: [%s])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->GetMsg().c_str());
   }
   
   ~CState2(void)
   {
      LogInfo("[%s][%u] [%s] destructed (current message: [%s])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->GetMsg().c_str());
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
   }
   
private:
   SPData m_spData;
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine and
 ** sends 1 event to it.
 **
 ***************************************************************************************/
int main (void)
{
   //EnableSerialLogDebug();
   LogInfo("[%s][%u] first-statemachine-with-data demo in\n", __FUNCTION__, __LINE__);
   
   //create a local scope so that the state machine is
   //destructed when it has handled the event
   try {
      //create the state machine
      LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
      TStateMachineWithData<CData> stateMachine(
                                                "state-machine",          //< name used for logging
                                                SPData(new CData()),      //< state machine takes ownership of the data instance and will destruct it
                                                ToState<CState1, CData>() //< used by the state machine to create the initial state
                                                );
      
      //handle first event
      //(local scoping event data)
      LogInfo("[%s][%u] send first event\n", __FUNCTION__, __LINE__);
      {
         int iEvtData = 1971;
         stateMachine.EventHandle(&iEvtData, 1);
      }
      
      //handle second event
      //(local scoping event data)
      LogInfo("[%s][%u] send second event\n", __FUNCTION__, __LINE__);
      {
         int iEvtData = 2018;
         stateMachine.EventHandle(&iEvtData, 1);
      }

      //finished sending events
      LogInfo("[%s][%u] sending events done\n", __FUNCTION__, __LINE__);
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] first-statemachine-with-data demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] first-statemachine-with-data demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

