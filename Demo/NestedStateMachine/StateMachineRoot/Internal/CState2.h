/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2028 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __DemoNestedStateMachineRoot_CState2_H__
#define __DemoNestedStateMachineRoot_CState2_H__

#include "CStateEvtId.h"

#include "CEventChildData.h"
#include "CStateMachineChild.h"

#include "CEventRootData.h"

#include "CData.h"

namespace DemoNestedStateMachineRoot {
   namespace Internal {
      class CState2 : public ILULibStateMachine::CStateEvtId {
         public:
                                                             CState2(SPData spData);
                                                             ~CState2(void);

         public:
            void                                             HandlerEvtTypeChildI(ILULibStateMachine::SPEventBase spEventBase, const int*                                           const pEvtData);
            void                                             HandlerEvtTypeChildE(ILULibStateMachine::SPEventBase spEventBase, const DemoNestedStateMachineEvents::CEventChildData* const pEvtData);
   
         private:
            SPData                                           m_spData;
            DemoNestedStateMachineChild::CStateMachineChild  m_StateMachineChild;
      };
   };
};

#endif //__DemoNestedStateMachineRoot_CState2_H__

