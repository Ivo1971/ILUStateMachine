/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CCreateStateFinished.h"
#include "CStateChangeException.h"
#include "ToState.h"

#include "CEventChildData.h"
#include "EEventChild.h"
#include "EEventRoot.h"

#include "CState1.h"
#include "CState2.h"
#include "CState3.h"

using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineEvents;

namespace DemoNestedStateMachineRoot {
   namespace Internal {
      /****************************************************************************************
       ** 
       ** CState1
       **
       ***************************************************************************************/
      CState1::CState1(SPData spData)
         : CStateEvtId("root-state-1")
         , m_spData(spData)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());

         EventRegister(HANDLER(CEventRootData, CState1, HandlerEvt1), ToState<CState2, CData>(), EEventRoot1);
      }
      
      CState1::~CState1(void)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
      }

      /****************************************************************************************
       ** 
       ** CState2
       **
       ***************************************************************************************/
      CState2::CState2(SPData spData)
         : CStateEvtId("root-state-2")
         , m_spData(spData)
         , m_StateMachineChild()
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         EventTypeRegister(HANDLER_TYPE(EEventChild, CEventChildData, CState2, HandlerEvtTypeChildE), NO_STATE_CHANGE);
         EventTypeRegister(HANDLER_TYPE(EEventChild, int,             CState2, HandlerEvtTypeChildI), NO_STATE_CHANGE);
      }
      
      CState2::~CState2(void)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
      }

      /****************************************************************************************
       ** 
       ** CState3
       **
       ***************************************************************************************/
      CState3::CState3(SPData spData)
         : CStateEvtId("root-state-3")
         , m_spData(spData)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());

         EventRegister(HANDLER(CEventRootData, CState3, HandlerEvt3), NO_STATE_CHANGE, EEventRoot3);
      }
      
      CState3::~CState3(void)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
      }
   };      
};
   
