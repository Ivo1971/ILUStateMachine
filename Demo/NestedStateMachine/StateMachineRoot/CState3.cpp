/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CState3.h"

using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineEvents;

namespace DemoNestedStateMachineRoot {
   namespace Internal {
      void CState3::HandlerEvt3(const CEventRootData* const pData)
      {
         LogInfo("%s %s data [%d]\n", GetName().c_str(), __FUNCTION__, pData->Get());
         m_spData->m_strMsg = "CState3::HandlerEvt3 called";
      }
   };
};
   
