/** @file
 ** @brief CEventRootData class declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __DemoNestedStateMachineEvents_CEventRootData_H__
#define __DemoNestedStateMachineEvents_CEventRootData_H__

namespace DemoNestedStateMachineEvents {
   class CEventRootData {
   public:
                  CEventRootData(const int iVal);

   public:
      void        Set(const int iVal);
      int         Get(void) const;
      
   private:
      int          m_iVal;
   };
}

#endif //#ifndef __DemoNestedStateMachineEvents_CEventRootData_H__

