/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>
using namespace std;

//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Track statemachine internals with some ugly global variables.
 ** After all, it is just a test application...
 **
 ***************************************************************************************/
namespace {
   enum EEventHandlers {
      EEventHandlerNone                       =  0,
      EEventHandlerEvtType1CState1Evt1Data5   = 11,
      EEventHandlerEvtType1CState1Evt1Data10  = 12,
      EEventHandlerEvtType1CState1Evt2Data5   = 13,
      EEventHandlerEvtType1CState1Evt2Data10  = 14,
      EEventHandlerEvtType1CState1Evt2        = 15,
      EEventHandlerEvtType1CState1            = 16,
      EEventHandlerEvtType1CStateDEvt3        = 21,
      EEventHandlerEvtType2CStateD            = 22
   };

   bool           g_bState1Constructed       = false;
   bool           g_bState1Destructed        = false;
   bool           g_bStateDefaultConstructed = false;
   bool           g_bStateDefaultDestructed  = false;
   EEventHandlers g_eEventHandlers           = EEventHandlerNone;
   int            g_iEventHandlerDataValue   = 0;
};

/****************************************************************************************
 ** 
 ** Help check functions.
 **
 ***************************************************************************************/
void CheckStatus(
                 const bool           bState1Constructed,
                 const bool           bState1Destructed,
                 const bool           bStateDefaultConstructed,
                 const bool           bStateDefaultDestructed,
                 const EEventHandlers eEventHandlers,
                 const int            iEventHandlerDataValue,
                 const int            iLine
                 )
{
   //check state 1 construction
   if(bState1Constructed != g_bState1Constructed) {
      stringstream ss;
      ss << "State 1 construction at [" << iLine << "] not as expected: value [" << g_bState1Constructed << "], expected  [" << bState1Constructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check state 1 destruction
   if(bState1Destructed != g_bState1Destructed) {
      stringstream ss;
      ss << "State 1 destruction at [" << iLine << "] not as expected: value [" << g_bState1Destructed << "], expected  [" << bState1Destructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check default state construction
   if(bStateDefaultConstructed != g_bStateDefaultConstructed) {
      stringstream ss;
      ss << "State Default construction at [" << iLine << "] not as expected: value [" << g_bStateDefaultConstructed << "], expected  [" << bStateDefaultConstructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check default state destruction
   if(bStateDefaultDestructed != g_bStateDefaultDestructed) {
      stringstream ss;
      ss << "State Default destruction at [" << iLine << "] not as expected: value [" << g_bStateDefaultDestructed << "], expected  [" << bStateDefaultDestructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check the event handler that has been called
   if(eEventHandlers != g_eEventHandlers) {
      stringstream ss;
      ss << "Eventhandler at [" << iLine << "] does not match expected handler: called [" << g_eEventHandlers << "], expected  [" << eEventHandlers << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check event handler data value
   if(iEventHandlerDataValue != g_iEventHandlerDataValue) {
      stringstream ss;
      ss << "Event handler data value mismatch at line [" << iLine << "]: value [" << g_iEventHandlerDataValue << "], expected [" << iEventHandlerDataValue << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //reset
   g_bState1Constructed       = false;
   g_bState1Destructed        = false;
   g_bStateDefaultConstructed = false;
   g_bStateDefaultDestructed  = false;
   g_eEventHandlers           = EEventHandlerNone;
   g_iEventHandlerDataValue   = 0;
}

/****************************************************************************************
 ** 
 ** Event enums.
 **
 ***************************************************************************************/
enum EEventsType1 {
   EEventsType1Id1 = 1,
   EEventsType1Id2 = 2,
   EEventsType1Id3 = 3,
   EEventsType1Id4 = 4
};

enum EEventsType2 {
   EEventsType2Id1 = 1,
};

enum EEventsType3 {
   EEventsType3Id1 = 1,
};

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CStateDefault;

/****************************************************************************************
 ** 
 ** First state
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1()
      : CStateEvtId("state-1")
   {
      /** register event handlers
       **/
      //handlers for event ID 1
      EventRegister(GUARD_HANDLER(int, CState1, GuardEvt1Data5,  HandlerEvt1Data5),      NO_STATE_CHANGE, EEventsType1Id1); //guarded handler
      EventRegister(GUARD_HANDLER(int, CState1, GuardEvt1Data10, HandlerEvt1Data10),     NO_STATE_CHANGE, EEventsType1Id1); //guarded handler
      //handlers for event ID 2
      EventRegister(GUARD_HANDLER(int, CState1, GuardEvt2Data5,  HandlerEvt2Data5),      NO_STATE_CHANGE, EEventsType1Id2); //guarded handler
      EventRegister(GUARD_HANDLER(int, CState1, GuardEvt2Data10, HandlerEvt2Data10),     NO_STATE_CHANGE, EEventsType1Id2); //guarded handler
      EventRegister(      HANDLER(int, CState1,                  HandlerEvt2),           NO_STATE_CHANGE, EEventsType1Id2); //unguarded handler (catch-all if none of the guards match for event ID 2)
      //handlers for event type EEventsType1
      EventTypeRegister(HANDLER_TYPE(EEventsType1, int, CState1, HandleEvtEEventsType1), NO_STATE_CHANGE);                  //type handler for 'EEventsType1' (catch-all if none of the other guarded and unguarded handlers for this type match)
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Constructed = true;
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Destructed = true;
   }
   
public:
   bool GuardEvt1Data5(const int* const pEvtData)
   {
      const bool bHandle = 5 == *pEvtData;
      LogInfo("GuardEvt1Data5 with data [%d]: %s\n", *pEvtData, bHandle ? "match" : "mismatch");
      return bHandle;
   }
   
   bool GuardEvt1Data10(const int* const pEvtData)
   {
      const bool bHandle = 10 == *pEvtData;
      LogInfo("GuardEvt1Data10 with data [%d]: %s\n", *pEvtData, bHandle ? "match" : "mismatch");
      return bHandle;
   }
   
   void HandlerEvt1Data5(const int* const pEvtData)
   {
      LogInfo("HandlerEvt1Data5 with data [%d]\n", *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CState1Evt1Data5;
   }

   void HandlerEvt1Data10(const int* const pEvtData)
   {
      LogInfo("HandlerEvt1Data10 with data [%d]\n", *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CState1Evt1Data10;
   }

   bool GuardEvt2Data5(const int* const pEvtData)
   {
      const bool bHandle = 5 == *pEvtData;
      LogInfo("GuardEvt2Data5 with data [%d]: %s\n", *pEvtData, bHandle ? "match" : "mismatch");
      return bHandle;
   }
   
   bool GuardEvt2Data10(const int* const pEvtData)
   {
      const bool bHandle = 10 == *pEvtData;
      LogInfo("GuardEvt2Data10 with data [%d]: %s\n", *pEvtData, bHandle ? "match" : "mismatch");
      return bHandle;
   }
   
   void HandlerEvt2Data5(const int* const pEvtData)
   {
      LogInfo("HandlerEvt2Data5 with data [%d]\n", *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CState1Evt2Data5;
   }

   void HandlerEvt2Data10(const int* const pEvtData)
   {
      LogInfo("HandlerEvt2Data10 with data [%d]\n", *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CState1Evt2Data10;
   }

   void HandlerEvt2(const int* const pEvtData)
   {
      LogInfo("HandlerEvt2 with data [%d]\n", *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CState1Evt2;
   }

   void HandleEvtEEventsType1(SPEventBase spEvent, const int* const pEvtData)
   {
      LogInfo("HandleEvtEEventsType1 event ID [%s] with data [%d]\n", spEvent->GetId().c_str(), *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CState1;
   }
};

/****************************************************************************************
 ** 
 ** Default state
 **
 ** This state has 1 event handler. The event ID is an integer (value 2).
 **
 ***************************************************************************************/
/** Class definition of state-default
 **/
class CStateDefault : public CStateEvtId {
public:
   CStateDefault()
      : CStateEvtId("state-default")
   {
      //register event handlers
      //handlers for event ID 3
      EventRegister(      HANDLER(int, CStateDefault,                  HandlerEvt3),           NO_STATE_CHANGE, EEventsType1Id3); //unguarded handler
      //handlers for event type EEventsType2
      EventTypeRegister(HANDLER_TYPE(EEventsType2, int, CStateDefault, HandleEvtEEventsType2), NO_STATE_CHANGE);                  //type handler for 'EEventsType1' (catch-all if none of the other guarded and unguarded handlers for this type match)
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bStateDefaultConstructed = true;
   }
   
   ~CStateDefault(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bStateDefaultDestructed = true;
   }
   
public:
   void HandlerEvt3(const int* const pEvtData)
   {
      LogInfo("HandlerEvt3 with data [%d]\n", *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType1CStateDEvt3;
   }

   void HandleEvtEEventsType2(SPEventBase spEvent, const int* const pEvtData)
   {
      LogInfo("HandleEvtEEventsType2 event ID [%s] with data [%d]\n", spEvent->GetId().c_str(), *pEvtData);
      g_iEventHandlerDataValue = *pEvtData;
      g_eEventHandlers         = EEventHandlerEvtType2CStateD;
   }
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine and
 ** sends several events to it that invoke the guarded,
 ** unguarded and type handlers.
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] guarded-handlers demo in\n", __FUNCTION__, __LINE__);
   
   try {
      //create a local scope so that the state machine is
      //destructed when it has handled the event
      {
         //variables
         int iEvtData = 0;
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerNone, 0, __LINE__);
         
         //create the state machine
         LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
         CStateMachine stateMachine(
                                    "state-machine",         //< name used for logging
                                    ToState<CState1>(),      //< used by the state machine to create the initial state
                                    ToState<CStateDefault>() //< used by the state machine to create the default state
                                    );
         
         //check
         CheckStatus(true,  false, true,  false, EEventHandlerNone, 0, __LINE__);
         
         //handle event
         {
            const EEventsType1 evtId = EEventsType1Id1;
            iEvtData                 = 5;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke guarded HandlerEvt1Data5\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CState1Evt1Data5, iEvtData, __LINE__);
         
         //handle event
         {
            const EEventsType1 evtId = EEventsType1Id1;
            iEvtData                 = 10;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke guarded HandlerEvt1Data5\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CState1Evt1Data10, iEvtData, __LINE__);
         
         //handle event
         {
            const EEventsType1 evtId = EEventsType1Id1;
            iEvtData                 = 15;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke guarded HandlerEvt1Data5\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CState1, iEvtData, __LINE__);
         
         //handle event
         //(local scoping event data)
         {
            const EEventsType1 evtId = EEventsType1Id2;
            iEvtData                 = 5;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke guarded HandlerEvt2Data5\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CState1Evt2Data5, iEvtData, __LINE__);
         
         //handle event
         //(local scoping event data)
         {
            const EEventsType1 evtId = EEventsType1Id2;
            iEvtData                 = 10;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke guarded HandlerEvt2Data10\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CState1Evt2Data10, iEvtData, __LINE__);
         
         //handle event
         {
            const EEventsType1 evtId = EEventsType1Id2;
            iEvtData                 = 15;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke unguarded HandleEvt2\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CState1Evt2, iEvtData, __LINE__);
         
         //handle event
         {
            const EEventsType1 evtId = EEventsType1Id3;
            iEvtData                 = 5;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke handler in default state\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType1CStateDEvt3, iEvtData, __LINE__);
         
         //handle event
         {
            const EEventsType2 evtId = EEventsType2Id1;
            iEvtData                 = 5;
            LogInfo("[%s][%u] send event [%d] with data [%d] --> invoke type HandleEvtEEventsType2 in default state\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }
         
         //check
         CheckStatus(false, false, false, false, EEventHandlerEvtType2CStateD, iEvtData, __LINE__);
         
         //handle event
         {
            const EEventsType3 evtId = EEventsType3Id1;
            iEvtData                 = 5;
            LogInfo("[%s][%u] send EEvents event [%u] with data [%d] --> unhandled\n", __FUNCTION__, __LINE__, evtId, iEvtData);
            stateMachine.EventHandle(&iEvtData, evtId);
         }

         //check
         CheckStatus(false, false, false, false, EEventHandlerNone, 0, __LINE__);
         
         //finished sending events
         LogInfo("[%s][%u] sending events done\n", __FUNCTION__, __LINE__);
      }

      //check
      CheckStatus(false, true,  false, true,  EEventHandlerNone, 0, __LINE__);   
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] guarded-handlers demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] guarded-handlers demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

