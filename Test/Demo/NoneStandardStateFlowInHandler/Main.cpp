/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>
using namespace std;

//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Track statemachine internals with some ugly global variables.
 ** After all, it is just a test application...
 **
 ***************************************************************************************/
namespace {
   bool g_bState1Constructed = false;
   bool g_bState1Destructed  = false;
   bool g_bState2Constructed = false;
   bool g_bState3Constructed = false;
   bool g_bState3Destructed  = false;
};

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CState2;
class CState3;

/****************************************************************************************
 ** 
 ** First state
 **
 ** This state has 1 event handler. The event ID is an integer (value 1). 
 ** In the registration of this handler, the standard flow is to switch to the second
 ** state.
 ** But the handler throws a state-change-exception to switch to the third state.
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1()
      : CStateEvtId("state-1")
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CState1, HandlerEvt1), //< the event handler
                    ToState<CState2>(),                 //< the state transition: switch to state-2 when the handler returns
                    1                                   //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Constructed = true;
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Destructed = true;
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      throw CStateChangeException("none-standard state change", ToState<CState3>());
   }
};

/****************************************************************************************
 ** 
 ** Second state.
 **
 ** This state has no event handlers.
 **
 ***************************************************************************************/
/** Class definition of state-2
 **/
class CState2 : public CStateEvtId {
public:
   CState2()
      : CStateEvtId("state-2")
   {
      //no event handlers
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState2Constructed = true;
   }
};

/****************************************************************************************
 ** 
 ** Thirth state.
 **
 ** This state has no event handlers.
 **
 ***************************************************************************************/
/** Class definition of state-3
 **/
class CState3 : public CStateEvtId {
public:
   CState3()
      : CStateEvtId("state-3")
   {
      //no event handlers
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState3Constructed = true;
   }
   
   ~CState3(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState3Destructed = true;
   }
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine and
 ** sends 1 event to it.
 ** The event handler throws a state-change exception and 
 ** instead of changing to state-2 (as defined in the handler
 ** registration) it changes to state-3 (as defined in the
 ** state-change exception).
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] none-standard-state-flow-in-handler demo in\n", __FUNCTION__, __LINE__);
   
   //create a local scope so that the state machine is
   //destructed when it has handled the event
   try {
      //create the state machine
      LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
      CStateMachine stateMachine(
                                 "state-machine",   //< name used for logging
                                 ToState<CState1>() //< used by the state machine to create the initial state
                                 );
      
      //check
      //state 1 has to be constructed and not destructed
      //(this makes it the current state)
      if(!g_bState1Constructed) {
         throw runtime_error("State 1 not constructed");
      }
      if(g_bState1Destructed) {
         throw runtime_error("State 1 already destructed");
      }

      //check
      //state 2 should not be constructed
      if(g_bState2Constructed) {
         throw runtime_error("State 2 already constructed");
      }
      
      //check
      //state 3 should not be constructed or destructed
      if(g_bState3Constructed) {
         throw runtime_error("State 3 already constructed");
      }
      if(g_bState3Destructed) {
         throw runtime_error("State 3 already destructed");
      }

      //handle event
      //(local scoping event data)
      LogInfo("[%s][%u] send first event\n", __FUNCTION__, __LINE__);
      {
         int iEvtData = 5;
         stateMachine.EventHandle(&iEvtData, 1);
      }

       //check
      //state 1 has to be constructed and destructed
      if(!g_bState1Constructed) {
         throw runtime_error("State 1 not constructed");
      }
      if(!g_bState1Destructed) {
         throw runtime_error("State 1 not destructed");
      }

      //check
      //state 2 should not be constructed
      if(g_bState2Constructed) {
         throw runtime_error("State 2 already constructed");
      }
      
      //check
      //state 3 has to be constructed and not destructed
      //(this makes it the current state)
      if(!g_bState3Constructed) {
         throw runtime_error("State 3 not constructed");
      }
      if(g_bState3Destructed) {
         throw runtime_error("State 3 already destructed");
      }

     //finished sending events
      LogInfo("[%s][%u] sending events done\n", __FUNCTION__, __LINE__);
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] none-standard-state-flow-in-handler demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] none-standard-state-flow-in-handler demo demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

