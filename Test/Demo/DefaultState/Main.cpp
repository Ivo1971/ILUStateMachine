/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>
using namespace std;

//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Track statemachine internals with some ugly global variables.
 ** After all, it is just a test application...
 **
 ***************************************************************************************/
namespace {
   bool g_bState1Constructed              = false;
   bool g_bState1Destructed               = false;
   bool g_bState1EventHandlerCalled       = false;
   bool g_bStateDefaultConstructed        = false;
   bool g_bStateDefaultDestructed         = false;
   bool g_bStateDefaultEventHandlerCalled = false;
   int  g_iEventHandlerDataValue          = 0;
};

/****************************************************************************************
 ** 
 ** Help check functions.
 **
 ***************************************************************************************/
void CheckStatus(
                 const bool   bState1Constructed,
                 const bool   bState1Destructed,
                 const bool   bState1EventHandlerCalled,
                 const bool   bStateDefaultConstructed,
                 const bool   bStateDefaultDestructed,
                 const bool   bStateDefaultEventHandlerCalled,
                 const int    iEventHandlerDataValue,
                 const int    iLine
                 )
{
   //check state 1 construction
   if(bState1Constructed != g_bState1Constructed) {
      stringstream ss;
      ss << "State 1 construction at [" << iLine << "] not as expected: value [" << g_bState1Constructed << "], expected  [" << bState1Constructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check state 1 destruction
   if(bState1Destructed != g_bState1Destructed) {
      stringstream ss;
      ss << "State 1 destruction at [" << iLine << "] not as expected: value [" << g_bState1Destructed << "], expected  [" << bState1Destructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check state 1 handler
   if(bState1EventHandlerCalled != g_bState1EventHandlerCalled) {
      stringstream ss;
      ss << "State 1 handler at [" << iLine << "] not called as expected: called [" << g_bState1EventHandlerCalled << "], expected  [" << bState1EventHandlerCalled << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check default state construction
   if(bStateDefaultConstructed != g_bStateDefaultConstructed) {
      stringstream ss;
      ss << "State Default construction at [" << iLine << "] not as expected: value [" << g_bStateDefaultConstructed << "], expected  [" << bStateDefaultConstructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check default state destruction
   if(bStateDefaultDestructed != g_bStateDefaultDestructed) {
      stringstream ss;
      ss << "State Default destruction at [" << iLine << "] not as expected: value [" << g_bStateDefaultDestructed << "], expected  [" << bStateDefaultDestructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check default state handler
   if(bStateDefaultEventHandlerCalled != g_bStateDefaultEventHandlerCalled) {
      stringstream ss;
      ss << "State Default handler at [" << iLine << "] not called as expected: called [" << g_bStateDefaultEventHandlerCalled << "], expected  [" << bStateDefaultEventHandlerCalled << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check event handler data value
   if(iEventHandlerDataValue != g_iEventHandlerDataValue) {
      stringstream ss;
      ss << "Event handler data value mismatch at line [" << iLine << "]: value [" << g_iEventHandlerDataValue << "], expected [" << iEventHandlerDataValue << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //reset
   g_bState1Constructed              = false;
   g_bState1Destructed               = false;
   g_bState1EventHandlerCalled       = false;
   g_bStateDefaultConstructed        = false;
   g_bStateDefaultDestructed         = false;
   g_bStateDefaultEventHandlerCalled = false;
   g_iEventHandlerDataValue          = 0;
}

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CStateDefault;

/****************************************************************************************
 ** 
 ** First state
 **
 ** This state has 1 event handler. The event ID is an integer (value 1).
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1()
      : CStateEvtId("state-1")
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CState1, HandlerEvt1), //< the event handler
                    NO_STATE_CHANGE,                    //< the state transition: stay in this state
                    1                                   //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Constructed = true;
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Destructed = true;
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      g_bState1EventHandlerCalled = true;
      g_iEventHandlerDataValue    = *pEvtData;
   }
};

/****************************************************************************************
 ** 
 ** Default state
 **
 ** This state has 1 event handler. The event ID is an integer (value 2).
 **
 ***************************************************************************************/
/** Class definition of state-default
 **/
class CStateDefault : public CStateEvtId {
public:
   CStateDefault()
      : CStateEvtId("state-default")
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CStateDefault, HandlerEvt2), //< the event handler
                    NO_STATE_CHANGE,                          //< the state transition: stay in this state
                    2                                         //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bStateDefaultConstructed = true;
   }
   
   ~CStateDefault(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bStateDefaultDestructed = true;
   }
   
public:
   void HandlerEvt2(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      g_bStateDefaultEventHandlerCalled = true;
      g_iEventHandlerDataValue          = *pEvtData;
   }
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine with a current state and
 ** a default state.
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] default-state demo in\n", __FUNCTION__, __LINE__);
   
   try {
      //create a local scope so that the state machine is
      //destructed when it has handled the events
      {
         //check
         CheckStatus(false, false, false, false, false, false, 0, __LINE__);

         //create the state machine
         LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
         CStateMachine stateMachine(
                                    "state-machine",         //< name used for logging
                                    ToState<CState1>(),      //< used by the state machine to create the initial state
                                    ToState<CStateDefault>() //< used by the state machine to create the default state
                                    );
         
         //check
         CheckStatus(true,  false, false, true,  false, false, 0, __LINE__);

         //handle event
         const int iEvtData1 = 5;
         LogInfo("[%s][%u] send first event with data [%d]\n", __FUNCTION__, __LINE__, iEvtData1);
         stateMachine.EventHandle(&iEvtData1, 1);
         
         //check
         CheckStatus(false, false, true,  false, false, false, iEvtData1, __LINE__);

         //handle event
         const int iEvtData2 = 9;
         LogInfo("[%s][%u] send second event with data [%d]\n", __FUNCTION__, __LINE__, iEvtData2);
         stateMachine.EventHandle(&iEvtData2, 2);
         
         //check
         CheckStatus(false, false, false, false, false, true,  iEvtData2, __LINE__);

         //handle event
         const int iEvtData3 = 17;
         LogInfo("[%s][%u] send third event with data [%d]\n", __FUNCTION__, __LINE__, iEvtData3);
         stateMachine.EventHandle(&iEvtData3, 3);
         
         //check
         CheckStatus(false, false, false, false, false, false, 0, __LINE__);

         //finished sending events
         LogInfo("[%s][%u] sending events done\n", __FUNCTION__, __LINE__);
      }

      //check
      CheckStatus(false, true,  false, false, true,  false, 0, __LINE__);
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] default-state demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] default-state demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

