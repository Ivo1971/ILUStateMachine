/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __DemoNestedStateMachineRoot_StateMachineRoot_H__
#define __DemoNestedStateMachineRoot_StateMachineRoot_H__

#include "TStateMachineWithData.h"

namespace DemoNestedStateMachineRoot {
   /** This is the one and only direct acces to the state root state machine:
    ** the function to create it
    ** the state machine itself will take care of registering event handlers,
    ** creating states, creating data ...
    **/
   ILULibStateMachine::SPStateMachine CreateStateMachine(void);

   /** Call this function to check the state of the root state machine
    ** against the expected values.
    **/
   void RootCheckStatus(
                        const bool   bState1Constructed,
                        const bool   bState1Destructed,
                        const bool   bState1EventHandlerCalled,
                        const bool   bState2Constructed,
                        const bool   bState2Destructed,
                        const bool   bState2EventHandlerCalled,
                        const bool   bState3Constructed,
                        const bool   bState3Destructed,
                        const bool   bState3EventHandlerCalled,
                        const int    iLine
                        );

   /** Call this function to check the state of the child state machine
    ** against the expected values.
    **/
   void ChildCheckStatus(
                         const int  iNbrInstances             ,
                         const bool bState1Constructed        = false,
                         const bool bState1Destructed         = false,
                         const bool bState1EventHandlerCalled = false,
                         const bool bState2Constructed        = false,
                         const bool bState2Destructed         = false,
                         const bool bState2EventHandlerCalled = false,
                         const bool bState3Constructed        = false,
                         const bool bState3Destructed         = false,
                         const bool bState3EventHandlerCalled = false,
                         const int  iLine                     = 0
                         );
};

#endif //__DemoNestedStateMachineRoot_StateMachineRoot_H__

