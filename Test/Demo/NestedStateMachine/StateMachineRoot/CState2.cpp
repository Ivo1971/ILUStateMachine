/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>

#include "ToState.h"

#include "CState2.h"
#include "CState3.h"

using namespace std;
using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineEvents;

namespace DemoNestedStateMachineRoot {
   /****************************************************************************************
    ** 
    ** Track statemachine internals with some ugly global variables.
    ** After all, it is just a test application...
    **
    ***************************************************************************************/
   namespace {
      bool g_bState2EventHandlerCalled = false;
   };

   /****************************************************************************************
    ** 
    ** Help check functions.
    **
    ***************************************************************************************/
   namespace Internal {
      void State2CheckStatus(
                             const bool bState2EventHandlerCalled,
                             const int  iLine
                             )
      {
         //check event handler called
         if(bState2EventHandlerCalled != g_bState2EventHandlerCalled) {
            stringstream ss;
            ss << "State 2 handler at [" << iLine << "] not called as expected: called [" << g_bState2EventHandlerCalled << "], expected  [" << bState2EventHandlerCalled << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }

         //reset
         g_bState2EventHandlerCalled = false;
      }
   };
   
   /****************************************************************************************
    ** 
    ** CState2 event handlers.
    **
    ***************************************************************************************/
   namespace Internal {
      void CState2::HandlerEvtTypeChild(SPEventBase spEventBase, const CEventChildData* const pEvtData)
      {
         //test bookkeeping
         g_bState2EventHandlerCalled = true;
         
         //forward the event to the child state machine
         //and check it has finished
         LogInfo("%s %s forwarding event to child\n", GetName().c_str(), __FUNCTION__);
         if(!m_StateMachineChild.EventHandle(pEvtData, spEventBase)) {
            //child not yet finished
            LogInfo("%s %s child not finished\n", GetName().c_str(), __FUNCTION__);
            return;
         }

         //child finished
         //get the info from the child state machine
         //(without accessing its internal state machine data instance)
         const std::string strInfoChild(m_StateMachineChild.InfoGet());
         //log what happened
         LogInfo("%s %s child finished with info [%s] --> to root state 3\n", GetName().c_str(), __FUNCTION__, strInfoChild.c_str());
         //state-change-exception to break default state flow (which remains in this state)
         //and switch to state-3
         throw CStateChangeException("state change to root state 3", ToState<CState3, CData>());
      }
   };
};
   
