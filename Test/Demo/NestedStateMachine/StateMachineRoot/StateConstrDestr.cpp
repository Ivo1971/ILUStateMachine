/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>

#include "CCreateStateFinished.h"
#include "CStateChangeException.h"
#include "ToState.h"

#include "CEventChildData.h"
#include "EEventChild.h"
#include "EEventRoot.h"

#include "CState1.h"
#include "CState2.h"
#include "CState3.h"
#include "StateConstrDestr.h"

using namespace std;
using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineEvents;

namespace DemoNestedStateMachineRoot {
   /****************************************************************************************
    ** 
    ** Track statemachine internals with some ugly global variables.
    ** After all, it is just a test application...
    **
    ***************************************************************************************/
   namespace {
      bool g_bState1Constructed = false;
      bool g_bState1Destructed  = false;
      bool g_bState2Constructed = false;
      bool g_bState2Destructed  = false;
      bool g_bState3Constructed = false;
      bool g_bState3Destructed  = false;
   };

   /****************************************************************************************
    ** 
    ** Check functions.
    **
    ***************************************************************************************/
   namespace Internal {
      void CheckStatus(
                       const bool   bState1Constructed,
                       const bool   bState1Destructed,
                       const bool   bState1EventHandlerCalled,
                       const bool   bState2Constructed,
                       const bool   bState2Destructed,
                       const bool   bState2EventHandlerCalled,
                       const bool   bState3Constructed,
                       const bool   bState3Destructed,
                       const bool   bState3EventHandlerCalled,
                       const int    iLine
                       )
      {
         //check state 1 construction
         if(bState1Constructed != g_bState1Constructed) {
            stringstream ss;
            ss << "State 1 construction at [" << iLine << "] not as expected: value [" << g_bState1Constructed << "], expected  [" << bState1Constructed << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }
         
         //check state 1 destruction
         if(bState1Destructed != g_bState1Destructed) {
            stringstream ss;
            ss << "State 1 destruction at [" << iLine << "] not as expected: value [" << g_bState1Destructed << "], expected  [" << bState1Destructed << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }
         
         //check state 1 event handler
         Internal::State1CheckStatus(bState1EventHandlerCalled, iLine);
         
         //check state 2 construction
         if(bState2Constructed != g_bState2Constructed) {
            stringstream ss;
            ss << "State 2 construction at [" << iLine << "] not as expected: value [" << g_bState2Constructed << "], expected  [" << bState2Constructed << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }
         
         //check state 2 destruction
         if(bState2Destructed != g_bState2Destructed) {
            stringstream ss;
            ss << "State 2 destruction at [" << iLine << "] not as expected: value [" << g_bState2Destructed << "], expected  [" << bState2Destructed << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }
         
         //check state 2 event handler
         Internal::State2CheckStatus(bState2EventHandlerCalled, iLine);
         
         //check state 3 construction
         if(bState3Constructed != g_bState3Constructed) {
            stringstream ss;
            ss << "State 3 construction at [" << iLine << "] not as expected: value [" << g_bState3Constructed << "], expected  [" << bState3Constructed << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }
         
         //check state 3 destruction
         if(bState3Destructed != g_bState3Destructed) {
            stringstream ss;
            ss << "State 3 destruction at [" << iLine << "] not as expected: value [" << g_bState3Destructed << "], expected  [" << bState3Destructed << "] (line [" << __LINE__ << "])";
            throw runtime_error(ss.str());
         }
         
         //check state 3 event handler
         Internal::State3CheckStatus(bState3EventHandlerCalled, iLine);
         
         //reset
         g_bState1Constructed = false;
         g_bState1Destructed  = false;
         g_bState2Constructed = false;
         g_bState2Destructed  = false;
         g_bState3Constructed = false;
         g_bState3Destructed  = false;
      }
   };
   
   /****************************************************************************************
    ** 
    ** State construction/destruction.
    **
    ***************************************************************************************/
   namespace Internal {
      /****************************************************************************************
       ** 
       ** CState1
       **
       ***************************************************************************************/
      CState1::CState1(SPData spData)
         : CStateEvtId("root-state-1")
         , m_spData(spData)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         EventRegister(HANDLER(CEventRootData, CState1, HandlerEvt1), ToState<CState2, CData>(), EEventRoot1);
         g_bState1Constructed = true;
      }
      
      CState1::~CState1(void)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         g_bState1Destructed = true;
      }

      /****************************************************************************************
       ** 
       ** CState2
       **
       ***************************************************************************************/
      CState2::CState2(SPData spData)
         : CStateEvtId("root-state-2")
         , m_spData(spData)
         , m_StateMachineChild()
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         EventTypeRegister(HANDLER_TYPE(EEventChild, CEventChildData, CState2, HandlerEvtTypeChild), NO_STATE_CHANGE);
         g_bState2Constructed = true;
      }
      
      CState2::~CState2(void)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         g_bState2Destructed = true;
      }

      /****************************************************************************************
       ** 
       ** CState3
       **
       ***************************************************************************************/
      CState3::CState3(SPData spData)
         : CStateEvtId("root-state-3")
         , m_spData(spData)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         EventRegister(HANDLER(CEventRootData, CState3, HandlerEvt3), NO_STATE_CHANGE, EEventRoot3);
         g_bState3Constructed = true;
      }
      
      CState3::~CState3(void)
      {
         LogInfo("%s '%s' with message [%s]\n", GetName().c_str(), __FUNCTION__, m_spData->m_strMsg.c_str());
         g_bState3Destructed = true;
      }
   };      
};
   
