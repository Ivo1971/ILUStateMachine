/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __DemoNestedStateMachineChild_StateConstrDestr_H__
#define __DemoNestedStateMachineChild_StateConstrDestr_H__

namespace DemoNestedStateMachineChild {
   namespace Internal {
      void CheckStatus(
                       const bool   bState1Constructed,
                       const bool   bState1Destructed,
                       const bool   bState1EventHandlerCalled,
                       const bool   bState2Constructed,
                       const bool   bState2Destructed,
                       const bool   bState2EventHandlerCalled,
                       const bool   bState3Constructed,
                       const bool   bState3Destructed,
                       const bool   bState3EventHandlerCalled,
                       const int    iLine
                       );
   };
};

#endif //#ifndef __DemoNestedStateMachineChild_StateConstrDestr_H__

