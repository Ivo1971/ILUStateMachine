/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __StateMachineChild_CState3__H__
#define __StateMachineChild_CState3__H__

#include "CStateEvtId.h"

#include "EventType2.h"

namespace StateMachineChild {
   namespace Internal {
      class CChildState3 : public ILULibStateMachine::CStateEvtId {
         public:
                         CChildState3();
                         ~CChildState3();

         public:
            void         HandlerEvt3(const LibEvents::CEventType2Data* const pData);

         private:
                         CChildState3(const CChildState3& ref);
            CChildState3 operator=(const CChildState3& ref);
      };
   };
};

#endif //__StateMachineChild_CChildState3__H__

