/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "ToState.h"

#include "CState2.h"
#include "CState4.h"

namespace StateMachineRoot {
   namespace Internal {
      void CState4::HandleEventType2ToChild(ILULibStateMachine::SPEventBase  spEventBase, const LibEvents::CEventType2Data* const pData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] CState2 glob data [%u] --> forward event\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         if(!m_spStateMachine->EventHandle(pData, spEventBase)) {
            ILULibStateMachine::LogInfo("[%s][%u] CState2 glob data [%u]: not finished\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
            return;
         }
         ILULibStateMachine::LogInfo("[%s][%u] CState2 glob data [%u]: finished --> to state 2\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         throw ILULibStateMachine::CStateChangeException("state change to state-2", ILULibStateMachine::ToState<CState2, CData>());
      }
   };
};

