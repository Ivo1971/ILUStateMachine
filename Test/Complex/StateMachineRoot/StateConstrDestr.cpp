/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 ** This file groups all state constructors and destructors of StateMachineRoot so that
 ** there is a clear overview of:
 ** - All event handlers (registered in state constructors);
 ** - All on-entry functions (called from state constructors);
 ** - All on-exit functions (called from state destructors).
 **
 **/
#include "CCreateStateFinished.h"
#include "CStateChangeException.h"
#include "ToState.h"

#include "StateMachineChild.h"

#include "CState1.h"
#include "CState2.h"
#include "CState3.h"
#include "CState4.h"
#include "CStateDefault.h"

namespace StateMachineRoot {
   namespace Internal {
      /****************************************************************************************
       ** 
       ** CState1
       **
       ***************************************************************************************/
      CState1::CState1(TYPESEL::shared_ptr<CData> spData)
         : ILULibStateMachine::CStateEvtId("state-1")
         , m_spData(spData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         //step 1: register event handlers
         EventRegister(HANDLER_EVT(CState1, HandlerEvt1), ILULibStateMachine::SPCreateState(),           LibEvents::EEvent1);
         EventRegister(HANDLER_EVT(CState1, HandlerEvt2), ILULibStateMachine::ToState<CState2, CData>(), LibEvents::EEvent2);
         EventRegister(HANDLER_EVT(CState1, HandlerEvt5), ILULibStateMachine::SPCreateState(),           LibEvents::EEvent5);
         EventRegister(HANDLER_EVT(CState1, HandlerEvt6), ILULibStateMachine::SPCreateState(),           LibEvents::EEvent6);
         EventRegister(HANDLER_EVT(CState1, HandlerEvt7), ILULibStateMachine::ToState<CState3, CData>(), LibEvents::EEvent7);
         
         //step 2: call on-entry functions this state
      }
      
      CState1::~CState1(void)
      {
         //step 1: call on-exit functions this state
         
         //step 2: unregister event handlers
         //        --> EventMachine::CStateEventMachine
         
         //testing 1-2
         m_spData->m_uiGlobVal += 10;
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
      }

      /****************************************************************************************
       ** 
       ** CState2
       **
       ***************************************************************************************/
      CState2::CState2(TYPESEL::shared_ptr<CData> spData)
         : ILULibStateMachine::CStateEvtId("state-2")
         , m_spData(spData)
         , m_uiGuardEvt1Cnt(0)
      {
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         //step 1: register event handlers
         EventRegister(GUARD_HANDLER_EVT(CState2, GuardEvt1Never,       HandlerEvt1Never),       ILULibStateMachine::SPCreateState(),             LibEvents::EEvent1);
         //EventRegister(GUARD_HANDLER_EVT(CState2, GuardEvt1SwitchState, HandlerEvt1SwitchState), ILULibStateMachine::ToState<CState1, CData>(),   LibEvents::EEvent1);
         EventRegister(      HANDLER_EVT(CState2, HandlerEvt1Default),                           ILULibStateMachine::SPCreateState(),             LibEvents::EEvent1);
         EventRegister(      HANDLER_EVT(CState2, HandlerEvt2),                                  ILULibStateMachine::ToState<CState1, CData>(),   LibEvents::EEvent2);
         EventRegister(      HANDLER_EVT(CState2, HandlerEvtAny),                                ILULibStateMachine::SPCreateState(),             LibEvents::EEvent3);
         EventRegister(      HANDLER_EVT(CState2, HandlerEvtAny),                                ILULibStateMachine::GetCreateStateFinished(),    LibEvents::EEvent8);
         
         //step 2: call on-entry functions this state
      }
      
      CState2::~CState2(void)
      {
         //step 1: call on-exit functions this state
         
         //step 2: unregister event handlers
         //        --> EventMachine::CStateEventMachine
         
         //testing 1-2
         m_spData->m_uiGlobVal += 25;
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
      }

      /****************************************************************************************
       ** 
       ** CState3
       **
       ***************************************************************************************/
      CState3::CState3(TYPESEL::shared_ptr<CData> spData)
         : ILULibStateMachine::CStateEvtId("state-3")
         , m_spData(spData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         //throw exception
         throw ILULibStateMachine::CStateChangeException("throw CStateChangeException", ILULibStateMachine::ToState<CState2, CData>());
      }
      
      CState3::~CState3(void)
      {
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
      }

      /****************************************************************************************
       ** 
       ** CState4
       **
       ***************************************************************************************/
      CState4::CState4(TYPESEL::shared_ptr<CData> spData)
         : ILULibStateMachine::CStateEvtId("state-4")
         , m_spData(spData)
         , m_spStateMachine(StateMachineChild::CreateStateMachine())
      {
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         //step 1: register event handlers
         EventTypeRegister(HANDLER_TYPE(LibEvents::EEvents, LibEvents::CEventType2Data, CState4, HandleEventType2ToChild), ILULibStateMachine::SPCreateState());

         //step 2: call on-entry functions this state
      }
      
      CState4::~CState4(void)
      {
         //step 1: call on-exit functions this state
         
         //step 2: unregister event handlers
         //        --> EventMachine::CStateEventMachine
         
         //testing 1-2
         m_spData->m_uiGlobVal += 25;
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
      }

      /****************************************************************************************
       ** 
       ** CStateDefault
       **
       ***************************************************************************************/
      CStateDefault::CStateDefault(TYPESEL::shared_ptr<CData> spData)
         : ILULibStateMachine::CStateEvtId("default state")
         , m_spData(spData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
         //step 1: register event handlers
         EventRegister(HANDLER_EVT(CStateDefault, HandlerEvt3), ILULibStateMachine::SPCreateState(),           LibEvents::EEvent3);
         EventRegister(HANDLER_EVT(CStateDefault, HandlerEvt9), ILULibStateMachine::ToState<CState4, CData>(), LibEvents::EEvent9);
         
         //step 2: call on-entry functions this state
      }
      
      CStateDefault::~CStateDefault(void)
      {
         //step 1: call on-exit functions this state
         
         //step 2: unregister event handlers
         //        --> EventMachine::CStateEventMachine
         
         //testing 1-2
         ILULibStateMachine::LogInfo("[%s][%u] data [%u]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal);
      }      
   };      
};
   
