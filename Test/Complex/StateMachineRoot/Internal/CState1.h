/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __StateMachineRoot_CState1__H__
#define __StateMachineRoot_CState1__H__

#include "CStateEvtId.h"

#include "Events.h"

#include "CData.h"

namespace StateMachineRoot {
   namespace Internal {
      class CState1 : public ILULibStateMachine::CStateEvtId {
         public:
                                       CState1(TYPESEL::shared_ptr<CData> spData);
                                       ~CState1(void);

         public:
            void                       HandlerEvt1(const LibEvents::CEventData* const pData);
            void                       HandlerEvt2(const LibEvents::CEventData* const pData);
            void                       HandlerEvt5(const LibEvents::CEventData* const pData);
            void                       HandlerEvt6(const LibEvents::CEventData* const pData);
            void                       HandlerEvt7(const LibEvents::CEventData* const pData);

         private:
                                       CState1(const CState1& ref);
            CState1                    operator=(const CState1& ref);

         private:
            TYPESEL::shared_ptr<CData> m_spData;
      };
   };
};

#endif //__StateMachineRoot_CState1__H__

