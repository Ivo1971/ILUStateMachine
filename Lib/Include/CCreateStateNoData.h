/** @file
 ** @brief The CCreateStateNoData declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CCreateStateNoData__H__
#define __ILULibStateMachine_CCreateStateNoData__H__

#include "CCreateState.h"

namespace ILULibStateMachine {
   /** Prototype of function to create a new state
    ** this cannot be a mere typedef since states can require
    ** constructor parameters. Boost bind can then be used to return
    ** an FCreateState with only the statemachine weak pointer 
    ** (parameters provided in the bind call) and keep this 
    ** transparent for the state machine engine.
    ** A parameter which is often required is the global state 
    ** machine data (CStateMachineData). It cannot be a parameter 
    ** of the FCreateState function to avoid a dynamic cast.
    **/
   typedef TYPESEL::function<CState*()> FCreateStateNoData;

   /** @brief Wrapper of a create-state function so it is possible to check that the function is valid or not. 
    **
    ** A TYPESEL::function cannot be checked for validity.
    ** Nevertheless, event handler definitions not requireing 
    ** a state change have to be able to indicate this.
    ** Thus the CCreateState wraps the FCreateState together
    ** with a valid-flag.
    ** An alternative would have been a std::pair.
    **/
   class CCreateStateNoData : public CCreateState {
      public:
                            CCreateStateNoData(FCreateStateNoData fCreateStateNoData);         

      public:
         CState*            CreateState(void) const;

      private:
         FCreateStateNoData m_fCreateStateNoData; //< The function to be called to create the state.
   };
}

#endif //__ILULibStateMachine_CCreateState__H__

