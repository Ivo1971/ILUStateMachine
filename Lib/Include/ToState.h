/** @file
 ** @brief The TCreateState template function defintions.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_ToState__H__
#define __ILULibStateMachine_ToState__H__

#include "CCreateStateNoData.h"
#include "CStateMachine.h"
#include "TCreateStateWithData.h"
#include "Types.h"

namespace ILULibStateMachine {
   namespace {
      /** @brief Templates to ease implementation of the state creation functions.
       **
       ** The templates assume a simple state class type that has 1 parameter:
       ** - a weak pointer to the state machine the class belongs to.
       ** It can only be used by state machines having no data.
       **/
      template<class CStateType> CState* ToStateInstanceNoData()
      {
         return new CStateType();
      }

      /** @brief Templates to ease implementation of the state creation functions.
       **
       ** The templates assume a simple state class type that has 2 parameters:
       ** - a weak pointer to the state machine the class belongs to;
       ** - a raw pointer to a state machine data class.
       **/
      template<class CStateType, class CDataType> CState* ToStateInstance(TYPESEL::shared_ptr<CDataType> spData)
      {
         return new CStateType(spData);
      }
   };
   
   /** Wrapper template around TCreateStateInstance that binds
    ** the state create function as expected by the state machine
    ** CCreateState.
    **/
   template<class CStateType> SPCreateState ToState(void)
   {
      return SPCreateState(new CCreateStateNoData(ToStateInstanceNoData<CStateType>));
   }

   /** Wrapper template around TRegisterCreateStateWithDataInstance that binds
    ** the state create function as expected by the state machine
    ** CCreateState.
    **/
   template<class CStateType, class CDataType> SPCreateState ToState(void)
   {
      return SPCreateState(
         new TCreateStateWithData<CDataType>(
            TYPESEL::bind(
               &ToStateInstance<CStateType,CDataType>, 
               TYPESEL_PLACEHOLDERS_1
            )
         )
      );
   }
}

#endif //__ILULibStateMachine_ToState__H__

