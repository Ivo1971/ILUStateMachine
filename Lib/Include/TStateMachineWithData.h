/** @file
 ** @brief The TStateMachineWithData declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_TStateMachineWithData__H__
#define __ILULibStateMachine_TStateMachineWithData__H__

#include "CStateMachine.h"
#include "TCreateStateWithData.h"

namespace ILULibStateMachine {
   /** @brief This is the state machine engine extended with a state machine data class, maintaining state and handling events.
    **
    ** When a statemachine has completely finished, the m_pState can become nullptr.
    **
    ** The class takes ownership of the provided pStateMachineData: it will destruct it,
    ** creates and owns the states
    **
    ** When a statemachine has completely finished, the m_pState can become nullptr.
    ** The state machine data will still be accessible.
    **
    ** It is possible to subclass TStateMachineWithData in order to get selective access (e.g. processing result) to
    ** the state machine data via public members (it is recomended not to give direct access to the data instance).
    **/
   template<class TData> class TStateMachineWithData : public CStateMachine {
   public:
      /** This constructor uses the CStateMachine constructor with delayed state creation, thus it calls ConstructStates from
       ** its body.
       **
       ** The state creation process can require calls to the virtual CreateStateWithData.
       **
       ** Using delayed state creation is required because virtual functions cannot be called before low-level CStateMachine
       ** and TStateMachineWithData construction (constructor lists) has finished.
       **/
      TStateMachineWithData(const char* szName, TYPESEL::shared_ptr<TData> const spData, SPCreateState spCreateState, SPCreateState spCreateDefaultState = SPCreateState())
         : CStateMachine(szName)
         , m_spData(spData)
      {
         if(nullptr == m_spData.get()) {
            throw std::runtime_error("TStateMachineWithData cannot have a null data pointer");
         }
         ConstructStates(spCreateState, spCreateDefaultState);
      }

   protected:
      /** Called from the CStateMachine class when it has to create a state
       ** having a constructor with a TData parameter.
       **/
      virtual CState* CreateStateWithData(SPCreateState spCreateState)
      {
         TCreateStateWithData<TData>* const pCreateStateWithData = dynamic_cast<TCreateStateWithData<TData>*>(spCreateState.get());
         if(nullptr == pCreateStateWithData) {
            throw std::runtime_error("TStateMachineWithData::CreateState called with invalid SPCreateState");
         }
         return pCreateStateWithData->CreateState(m_spData);
      }
      
   protected:
      /** Reference to the state machine data instance.
       ** 
       ** Protected so it can be used by subclasses to shield its internals
       ** from consumers of the state machine.
       **
       ** Shared pointer because the same instance is shared with the CState classes
       ** that have a different lify cycle (the last CState is destructed in the destructor
       ** of the CStateMachine parent class, which is called after the destructor of
       ** this (TStateMachineWithData) has been called.
       **/
      TYPESEL::shared_ptr<TData> m_spData;
   };
}

#endif //__ILULibStateMachine_TStateMachineWithData__H__

