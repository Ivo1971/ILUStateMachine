/** @file
 ** @brief The CStateChangeException declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CStateChangeException_H__
#define __ILULibStateMachine_CStateChangeException_H__

#include "stdexcept"

#include "CCreateState.h"

namespace ILULibStateMachine {
   /** @brief When the state machine catches an exception of this type while executing an event handler
    ** or while constructing a new state, it will switch to the state created by calling the
    ** CCreateState instance in the caught exception.
    **
    ** This class allows breaking the normal flow as dictated by the registered event handlers.
    **/
   class CStateChangeException : public std::runtime_error {
      public:
                      CStateChangeException(const std::string& whatArg, SPCreateState spCreateState);
                      ~CStateChangeException(void) throw();

      public:
         SPCreateState GetCreateState(void) const;

      private:
         SPCreateState m_spCreateState; //< The state machine will use this instance to change the current state when it catches this exception.
   };
};

#endif //__ILULibStateMachine_CStateChangeException_H__

