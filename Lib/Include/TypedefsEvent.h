/** @file
 ** @brief CEventBase and CHandleEventInfoBase related typedefs.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_TypedefsEvent_H__
#define __ILULibStateMachine_TypedefsEvent_H__

#include "CEventBase.h"
#include "CHandleEventInfoBase.h"
#include "CSPEventBaseSort.h"

namespace ILULibStateMachine {
   typedef std::pair<SPEventBase, SPHandleEventInfoBase>                  EventPair;       ///< pair coupling an event ID to a handle-event-info instance
   typedef std::map<SPEventBase, SPHandleEventInfoBase, CSPEventBaseSort> EventMap;        ///< map of event ID/handle-event-info pairs, do use custom sort because we do not want to sort pointer values
   typedef EventMap::iterator                                             EventMapIt;      ///< iterator for the event map
   typedef EventMap::const_iterator                                       EventMapCIt;     ///< const iterator for the event map
   typedef std::pair<std::string, SPHandleEventInfoBase>                  EventTypePair;   ///< pair coupling an event ID to a handle-event-info instance
   typedef std::map<std::string, SPHandleEventInfoBase>                   EventTypeMap;    ///< map of event ID/handle-event-info pairs, do use custom sort because we do not want to sort pointer values
   typedef EventTypeMap::iterator                                         EventTypeMapIt;  ///< iterator for the event map
   typedef EventTypeMap::const_iterator                                   EventTypeMapCIt; ///< const iterator for the event map
};

#endif //__ILULibStateMachine_TypedefsEvent_H__

