/** @file
 ** @brief The CStateMachine template function defintions.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CStateMachineImpl__H__
#define __ILULibStateMachine_CStateMachineImpl__H__

#include "stdexcept"

#include "CStateEvtId.h"
#include "Logging.h"
#include "THandleEventInfo.h"
#include "THandleEventTypeInfo.h"

namespace ILULibStateMachine {
   /** Event handler, called when an event has to be fed into the state machine.
    **
    ** Constructs a SPEventBase instance based on the provided event parameters
    ** and calls the common handler with this instance.
    **/
   template <class TEventData, class EvtId>                                                    
   bool CStateMachine::EventHandle(
      const TEventData* const pEventData, //< The event data belonging to the event.
      const EvtId             evtId       //< Event ID as defined by TEventEvtId.
      )
   {
      return EventHandle(pEventData, SPEventBase(new TEventEvtId<EvtId>(typeid(TEventData), evtId)));
   }

   /** Event handler, called when an event has to be fed into the state machine.
    **
    ** Constructs a SPEventBase instance based on the provided event parameters
    ** and calls the common handler with this instance.
    **/
   template <class TEventData, class EvtId, class EvtSubId1>                                                    
   bool CStateMachine::EventHandle(
      const TEventData* const pEventData, //< The event data belonging to the event.
      const EvtId             evtId,      //< Event ID as defined by TEventEvtId.
      const EvtSubId1         evtSubId1   //< First event sub-ID as defined by TEventEvtId.
      )
   {
      return EventHandle(pEventData, SPEventBase(new TEventEvtId<EvtId, EvtSubId1>(typeid(TEventData), evtId, evtSubId1)));
   }
   
   /** Event handler, called when an event has to be fed into the state machine.
    **
    ** Constructs a SPEventBase instance based on the provided event parameters
    ** and calls the common handler with this instance.
    **/
   template <class TEventData, class EvtId, class EvtSubId1, class EvtSubId2>                                                    
   bool CStateMachine::EventHandle(
      const TEventData* const pEventData, //< The event data belonging to the event.
      const EvtId             evtId,      //< Event ID as defined by TEventEvtId.
      const EvtSubId1         evtSubId1,  //< First event sub-ID as defined by TEventEvtId.
      const EvtSubId2         evtSubId2   //< Second event sub-ID as defined by TEventEvtId.
      )
   {
      return EventHandle(pEventData, SPEventBase(new TEventEvtId<EvtId, EvtSubId1, EvtSubId2>(typeid(TEventData), evtId, evtSubId1, evtSubId2)));
   }
   
   /** Event handler, called when an event has to be fed into the state machine.
    **
    ** Constructs a SPEventBase instance based on the provided event parameters
    ** and calls the common handler with this instance.
    **/
   template <class TEventData, class EvtId, class EvtSubId1, class EvtSubId2, class EvtSubId3>                                                    
   bool CStateMachine::EventHandle(
      const TEventData* const pEventData, //< The event data belonging to the event.
      const EvtId             evtId,      //< Event ID as defined by TEventEvtId.
      const EvtSubId1         evtSubId1,  //< First event sub-ID as defined by TEventEvtId.
      const EvtSubId2         evtSubId2,  //< Second event sub-ID as defined by TEventEvtId.
      const EvtSubId3         evtSubId3   //< Third event sub-ID as defined by TEventEvtId.   
      )
   {
      return EventHandle(pEventData, SPEventBase(new TEventEvtId<EvtId, EvtSubId1, EvtSubId2, EvtSubId3>(typeid(TEventData), evtId, evtSubId1, evtSubId2, evtSubId3)));
   }

   /** Event handler, called when an event has to be fed into the state machine.
    **
    ** This function dictates the order in which the event is checked against registered event handlers
    ** - current state event handlers;
    ** - default state event handlers;
    ** - current state event-type handlers;
    ** - default state event-type handlers.
    ** Depending on the match, the appropriate handler is called.
    **
    ** When no match is found, the function traces all registered handlers.
    **
    ** @return true: when the state machine has finished (current state is null); false when the state machine still has a valid state (not null), meaning it has not finished
    **/
   template <class TEventData>                                                    
   bool CStateMachine::EventHandle(
      const TEventData* const pEventData, //< The event data belonging to the event.
      const SPEventBase       spEventBase //< Class instance describing the event in all detail (1 class instance instead of seperate parameters).
      )
   {
      //store the current state name as the current state can change and the logging
      //should keep the original state name for the handling loggings
      const std::string strCurrentState(GetStateName());
      
      CLogIndent logIndent;
      LogNotice("Statemachine [%s] state [%s] handling event [%s] type [%s] in\n",
                m_strName.c_str(),
                strCurrentState.c_str(),
                spEventBase->GetId().c_str(),
                spEventBase->GetDataType().c_str()
                );
      
      //try the state event map
      if(EventHandle(false, pEventData, spEventBase)) {
         //event handled
         LogNotice("Statemachine [%s] state [%s] handling event [%s] by current state done\n",
                   m_strName.c_str(),
                   strCurrentState.c_str(),
                   spEventBase->GetId().c_str()
                   );
         return HasFinished();
      }
      
      //try the default event map
      if(EventHandle(true, pEventData, spEventBase)) {
         //event handled
         LogNotice("Statemachine [%s] state [%s] handling event [%s] by default state done\n",
                   m_strName.c_str(),
                   strCurrentState.c_str(),
                   spEventBase->GetId().c_str()
            );
         return HasFinished();
      }

      //try the state type map
      if(EventTypeHandle(false, pEventData, spEventBase)) {
         //event handled
         LogNotice("Statemachine [%s] state [%s] handling event type [%s] by current state done\n",
                   m_strName.c_str(),
                   strCurrentState.c_str(),
                   spEventBase->GetDataType().c_str()
            );
         return HasFinished();
      }
      
      //try the default type map
      if(EventTypeHandle(true, pEventData, spEventBase)) {
         //event handled
         LogNotice("Statemachine [%s] state [%s] handling event type [%s] by default state done\n",
                   m_strName.c_str(),
                   strCurrentState.c_str(),
                   spEventBase->GetDataType().c_str()
                   );
         return HasFinished();
      }
      
      LogNotice("Statemachine [%s] state [%s] handling event [%s] looking for handler failed: no matching registered handler --> event ignored\n",
                m_strName.c_str(),
                strCurrentState.c_str(),
                spEventBase->GetId().c_str()
                );
      TraceAll();
      return HasFinished();
   }

   /** Internal event handler.
    **
    ** The state machine calls this function to find an event match (not an event-type)
    ** match and call the handler if a match is found.
    **
    ** @return true: when the event has been handled (false otherwise).
    **/
   template <class TEventData>                                                    
   bool CStateMachine::EventHandle(
      const bool              bDefault,   //< Use the current state (false) or the default state (true) to find a matching registered event.
      const TEventData* const pEventData, //< The event data belonging to the event.
      const SPEventBase       spEventBase //< Class instance describing the event in all detail (1 class instance instead of seperate parameters).
      )
   {
      //find handler
      EventMap* const pMap = EventGetMap(bDefault);
      if(nullptr == pMap) {
         return false;
      }
      LogDebug("Statemachine [%s] state [%s] handling event [%s] looking for [%s] handler (%lu registered ID's)\n",
               m_strName.c_str(),
               GetStateName().c_str(),
               spEventBase->GetId().c_str(),
               (bDefault ? "default" : "state"),
               (long unsigned int)pMap->size()
               );
      const EventMapIt it = pMap->find(spEventBase);
      if(pMap->end() == it) {
         return false;
      }

      //handler found --> get info to call it
      LogDebug("Statemachine [%s] state [%s] handling event [%s] found [%s] handler\n",
               m_strName.c_str(),
               GetStateName().c_str(),
               spEventBase->GetId().c_str(),
               (bDefault ? "default" : "state")
               );
      THandleEventInfo<TEventData>* pHandleEventInfo = dynamic_cast<THandleEventInfo<TEventData>*>(it->second.get());
      if(NULL == pHandleEventInfo) {
         //serious error in the implementation: mismatch in registration
         LogErr("Statemachine [%s] state [%s] handling event [%s] looking for [%s] handler found handler with invalid type\n",
                m_strName.c_str(),
                GetStateName().c_str(),
                spEventBase->GetId().c_str(),
                (bDefault ? "default" : "state")
                );
        return false;
      }
      
      //call handler
      CHandleEventInfoBase::HandleResult handleResult(pHandleEventInfo->Handle(bDefault, pEventData));
      if(!handleResult.first) {
         //no handler found
         return false;
      }
      
      //state change if requested by handler
      ChangeState(handleResult.second);
      
      //event handled
      return true;
   }

   /** Internal event handler.
    **
    ** The state machine calls this function to find an event-type (not an event)
    ** match and call the handler if a match is found.
    **
    ** @return true: when the event has been handled (false otherwise).
    **/
   template <class TEventData>                                                    
   bool CStateMachine::EventTypeHandle(
      const bool              bDefault,   //< Use the current state (false) or the default state (true) to find a matching registered event.
      const TEventData* const pEventData, //< The event data belonging to the event.
      const SPEventBase       spEventBase //< Class instance describing the event in all detail (1 class instance instead of seperate parameters).
      )
   {
      //find handler
      EventTypeMap* const pMap = EventTypeGetMap(bDefault);
      if(nullptr == pMap) {
         return false;
      }
      const std::string strIdTypeFull(CStateEvtId::ComposeTypeHandlerIdString(spEventBase->GetIdType(), typeid(TEventData)));
      LogDebug("Statemachine [%s] state [%s] handling event type [%s] in [%s] (%lu registered ID's)\n",
               m_strName.c_str(),
               GetStateName().c_str(),
               strIdTypeFull.c_str(),
               (bDefault ? "default" : "state"),
               (long unsigned int)pMap->size()
               );
      const EventTypeMapIt it = pMap->find(strIdTypeFull);
      if(pMap->end() == it) {
         return false;
      }

      //handler found --> get info to call it
      THandleEventTypeInfo<TEventData>* pHandleEventTypeInfo = dynamic_cast<THandleEventTypeInfo<TEventData>*>(it->second.get());
      if(NULL == pHandleEventTypeInfo) {
         //serious error in the implementation: mismatch in registration
         LogErr("Statemachine [%s] state [%s] handling event type [%s] in [%s] found type handler with type\n",
                  m_strName.c_str(),
                  GetStateName().c_str(),
                  strIdTypeFull.c_str(),
                  (bDefault ? "default" : "state")
                  );
         return false;
      }
      
      //call handler
      CHandleEventInfoBase::HandleResult handleResult(pHandleEventTypeInfo->Handle(bDefault, spEventBase, pEventData));
      if(!handleResult.first) {
         //no handler found
         return false;
      }
      
      //state change if requested by handler
      ChangeState(handleResult.second);
      
      //event handled
      return true;
   }
};
   
#endif //__ILULibStateMachine_CStateMachineImpl__H__

