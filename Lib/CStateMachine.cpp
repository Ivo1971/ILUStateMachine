/** @file
 ** @brief The CStateMachine declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CStateChangeException.h"
#include "CState.h"
#include "CStateMachine.h"
#include "Logging.h"

/** @brief This is the namespace that wraps the public interface of this library.
 **/
namespace ILULibStateMachine {
   /** Constructor.
    **/
   CStateMachine::CStateMachine(
      const char*              szName,              //< State machine name, logging only.
      SPCreateState            spCreateState,       //< Class to create the initial state.
      SPCreateState            spCreateDefaultState //< Class to create the default state.
      )
      : m_strName            (szName                           )
      , m_pDefaultState      (CreateStateCatchEx(spCreateDefaultState, false))
      , m_pState             (CreateStateCatchEx(spCreateState,        true ))
   {
      ConstructorCheck();
   }

   /** Constructor with delayed state creation.
    **
    ** Both the current state and default state are set to null.
    ** The subclass has to call itself 'ConstructStates' from its 
    ** constructor to ensure the state machine is in a valid state.
    ** This is NOT checked, this is up to the developer.
    **
    ** Also calling 'ConstructorCheck' is delayed untill 'ConstructStates'
    ** is called.
    **/
   CStateMachine::CStateMachine(
      const char*               szName              //< State machine name, logging only.
      )
      : m_strName            (szName       )
      , m_pDefaultState      (nullptr      )
      , m_pState             (nullptr      )
   {
   }
   
   /** Destructor.
    **/
   CStateMachine::~CStateMachine()
   {
      //delete in reverse order
      delete m_pState;
      delete m_pDefaultState;
   }

   /** Called once from all constructors to check the state machine has been constructed fine.
    **/
   void CStateMachine::ConstructorCheck(void)
   {
      if((nullptr == m_pState) && (nullptr == m_pDefaultState)) {
         throw std::runtime_error("Cannot create a state machine without an initial or a default state");
      } else if(nullptr == m_pState) {
         LogWarning("Creating a state machine without initial state (the default state is valid).\n");
      }
   }

   /** Get the state machine name.
    **
    ** @return the state machine name.
    **/
   const std::string& CStateMachine::GetName() const
   {
      return m_strName;
   }

   /** Indicates whether the state machine has finished (state is null) or not.
    **
    ** @return true: when the state machine has finished (state is null); false when the state machine still has a valid state (not null), meaning it has not finished.
    **/
   bool CStateMachine::HasFinished() const
   {
      return nullptr == m_pState;
   }

   /** Closely cooperates with the TStateMachineWithData subclass.
    **
    ** This is a virtual function, also implemented in TStateMachineWithData.
    ** When it is called here this means some application is trying to use
    ** states with state machine data but with a state machine that does not
    ** derrive from TStateMachineWithData.
    ** Since this is not possible, an exception is thrown.
    **/
   CState* CStateMachine::CreateStateWithData(SPCreateState)
   {
      throw std::runtime_error("Statemachines with states with data only supporting for statemachines deriving from TStateMachineWithData");
   }

   /** Create a state instance (CState) as dictated by the SPCreateState parameter.
    **/
   CState* CStateMachine::CreateState(
      SPCreateState spCreateState //< Class that describes the state to be created. When the shared-pointer is null no state has to be created and a nullptr is returned.
      )
   {
      //check if a state creation
      if(!spCreateState) {
         //nope, no state has to be created
         return nullptr;
      }
      //yes, a state has to be created
      
      LogDebug("StateMachine [%s] constructing new state\n", GetName().c_str());
      CState* pState = nullptr;
      {
         CLogIndent logIndent;
         if(!spCreateState->HasToCallCreateStateWithData()) {
            pState = spCreateState->CreateState();
         } else {
            pState = CreateStateWithData(spCreateState);
         }
      }
      if(nullptr == pState) {
         LogDebug("StateMachine [%s] constructing null state\n", GetName().c_str());
      } else {
         LogDebug("StateMachine [%s] constructing new state [%s] done\n", GetName().c_str(), pState->GetName().c_str());
      }
      return pState;
   }

   /** Create a state instance (CState) as dictated by the SPCreateState parameter and catch all exceptions.
    **/
   CState* CStateMachine::CreateStateCatchEx(
      SPCreateState spCreateState, //< Class that describes the state to be created. When the shared-pointer is null no state has to be created and a nullptr is returned.
      const bool bIsDefaultState   //< Indicates this is the default state or not (used for loggign only)
      )
   {
      try {
         return CreateState(spCreateState);
      } catch(std::exception& ex) {
         LogDebug("StateMachine [%s] create %s state caused an exception: %s\n", GetName().c_str(), bIsDefaultState ? "initial" : "default", ex.what());
         return nullptr;
      } catch(...) {
         LogDebug("StateMachine [%s] create %s state caused an exception: %s\n", GetName().c_str(), bIsDefaultState ? "initial" : "default", "unknown");
         return nullptr;
      }
   }
   
   /** This function constructs the required states after the protected constructor with delayed state creation has been used.
    **
    ** Since this is the last step in the construction process, 'ConstructorCheck' is called once the states have been
    ** created.
    **/
   void  CStateMachine::ConstructStates(SPCreateState spCreateState, SPCreateState spCreateDefaultState)
   {
      m_pDefaultState = CreateStateCatchEx(spCreateDefaultState, true );
      m_pState        = CreateStateCatchEx(spCreateState,        false);
      ConstructorCheck();
   }

   /** Take all actions required to change the current state of the state machine.
    **
    ** This includes desctructing the current state (and unregistering all its event
    ** handlers) and constructing the new state.
    **/
   void CStateMachine::ChangeState(
      SPCreateState spCreateState //< Class that describes the next state to be created. Class can describe that no new state has to be created, in which case the state machine remains in the same state.
      )
   {
      //step 1: check if a state change is required
      if(!spCreateState) {
         //nope, no state change requested
         return;
      }
      //yes, state change requested

      //step 2: delete existing state
      {
         const std::string strStateName(GetStateName(false));
         LogDebug("State-change destructing state [%s]\n", strStateName.c_str());
         {
            CLogIndent logIndent;
            delete m_pState;
         }
         LogDebug("State-change destructing state [%s] done\n", strStateName.c_str());
         m_pState = nullptr;
      }

      //step 3: create the new state
      //        store the new state
      //        when a 'CStateChangeException' occures, propagate to creating the next state
      for(SPCreateState spCreateStateLoop = spCreateState ; spCreateStateLoop ; /* createStateLoop changed inside the loop */) {
         try {
            SPCreateState spCreateStateTmp = spCreateStateLoop;
            spCreateStateLoop = SPCreateState(); //make invalid (break loop)
            m_pState = CreateState(spCreateStateTmp);
         } catch(CStateChangeException& ex) {
            LogWarning("Caught state-change-exception while creating new state --> create next state: %s\n", ex.what());
            spCreateStateLoop = ex.GetCreateState();
         } catch(std::exception& ex) {
            LogErr("Caught exeption while creating new state --> setting null-state (state machine finished): %s\n", ex.what());
            m_pState = nullptr;
         } catch(...) {
            LogErr("Caught exeption while creating new state --> setting null-state (state machine finished): %s\n", "unknown");
            m_pState = nullptr;
         }
      }
   }

   /** Get a reference to the event map for the current or default state.
    **/
   EventMap* CStateMachine::EventGetMap(
      const bool bDefault //< When true get a reference to the default map; when false get a reference to the current map.
      )
   {
      CState* const pState = bDefault ? m_pDefaultState : m_pState;
      if(nullptr == pState) {
         return nullptr;
      }
      return pState->EventGetMap();
   }

   /** Get a const reference to the event map for the current or default state.
    **/
   const EventMap* CStateMachine::EventGetMap(
      const bool bDefault //< When true get a reference to the default map; when false get a reference to the current map.
      ) const
   {
      const CState* const pState = bDefault ? m_pDefaultState : m_pState;
      if(nullptr == pState) {
         return nullptr;
      }
      return pState->EventGetMap();
   }

   /** Get a reference to the event-type map for the current or default state.
    **/
   EventTypeMap* CStateMachine::EventTypeGetMap(
      const bool bDefault //< When true get a reference to the default map; when false get a reference to the current map.
      )
   {
      CState* const pState = bDefault ? m_pDefaultState : m_pState;
      if(nullptr == pState) {
         return nullptr;
      }
      return pState->EventTypeGetMap();
   }

   /** Get a const reference to the event-type map for the current or default state.
    **/
   const EventTypeMap* CStateMachine::EventTypeGetMap(
      const bool bDefault //< When true get a reference to the default map; when false get a reference to the current map.
      ) const
   {
      const CState* const pState = bDefault ? m_pDefaultState : m_pState;
      if(nullptr == pState) {
         return nullptr;
      }
      return pState->EventTypeGetMap();
   }

   /** Trace all registered handlers.
    **/
   void CStateMachine::TraceAll() const
   {
      CLogIndent logIndent;
      LogDebug("Statemachine [%s] state [%s] registered handlers:\n",
               m_strName.c_str(),
               GetStateName().c_str()
               );
      TraceHandlers(false);
      TraceHandlers(true);
      TraceTypeHandlers(false);
      TraceTypeHandlers(true);
   }

   /** Trace all event handlers registered for the default or current state.
    **/
   void CStateMachine::TraceHandlers(
      const bool bDefault //< When true trace handlers belonging to the default state; when false trace handlers belonging to the current state.
      ) const
   {
      CLogIndent logIndent1;
      const EventMap* const pMap = EventGetMap(bDefault);
      if(nullptr == pMap) {
         return;
      }
      LogDebug("%s event handlers (%lu):\n", GetStateName(bDefault).c_str(), (long unsigned int)pMap->size());
      {
         CLogIndent logIndent2;
         for(EventMapCIt cit = pMap->begin() ; pMap->end() != cit ; ++cit) {
            LogDebug("ID [%s] event type [%s] with data type [%s])\n", cit->first->GetId().c_str(), cit->first->GetIdType().c_str(), cit->first->GetDataType().c_str());
         }
      }
   }

   /** Trace all event type handlers registered for the default or current state.
    **/
   void CStateMachine::TraceTypeHandlers(
      const bool bDefault //< When true trace handlers belonging to the default state; when false trace handlers belonging to the current state.
      ) const
   {
      CLogIndent logIndent1;
      const EventTypeMap* const pMap = EventTypeGetMap(bDefault);
      if(nullptr == pMap) {
         return;
      }
      LogDebug("%s event type handlers (%lu):\n", GetStateName(bDefault).c_str(), (long unsigned int)pMap->size());
      {
         CLogIndent logIndent2;
         for(EventTypeMapCIt cit = pMap->begin() ; pMap->end() != cit ; ++cit) {
            LogDebug("%s\n", cit->first.c_str());
         }
      }
   }

   /** Get the name of the default or current state.
    **
    ** The function handles state machines without a default state.
    ** The function handles state machines that have finished (no current state).
    **/
   std::string CStateMachine::GetStateName(
      const bool bDefault //< When true get the name of the default state; when false get the name of the current state.
      ) const
   {
      if(bDefault) {
         if(nullptr == m_pDefaultState) {
            return "no default state";
         }
         return m_pDefaultState->GetName();
      }
      if(nullptr == m_pState) {
         return "no current state";
      }
      return m_pState->GetName();
   }
} // namespace ILULibStateMachine

